/*
 * Menu_Lakalantas.java
 *
 * Created on September 3, 2013, 4:36 PM
 */

package skripsikarina;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import skripsikarina.entity.AtributLakaLantas;
import skripsikarina.entity.tableCellHasil;
import skripsikarina.helper.customCellRender;
import skripsikarina.helper.dbConnection;
import skripsikarina.helper.fuzzyLogic2;
import skripsikarina.helper.fuzzyLogicKhususMenuLakalantas;
/**
 *
 * @author  karin
 */
public class Menu_Lakalantas extends javax.swing.JFrame {
    public Connection kon;
    public Statement stat;
    public ResultSet rs;
    public String sql="";
    private DefaultTableModel model;
    private fuzzyLogicKhususMenuLakalantas fz2;
    ArrayList<String> convertToAngka = new ArrayList<String>();
    private AtributLakaLantas atributLakaLantas;
    private int UIKSize = 0; 
    private Object[] UIK1;
    private DefaultTableModel model2;
    private DefaultTableModel model3;
    /** Creates new form Menu_Lakalantas */
    //kosntruktor
    public Menu_Lakalantas(){
        initComponents();
        model=new DefaultTableModel();
        this.jTable1.setModel(model);
        model.addColumn("No.");
        model.addColumn("Tgl Kejadian");
        model.addColumn("Tempat Kejadian");
        model.addColumn("Nama Pelaku");
        model.addColumn("Nama Korban");
        model.addColumn("Lokasi");
        ambil_data_tabel();
    }
    private void caridata(){
        try {            
            //Class.forName("sun.jdbc.odbc.JdbcOdbc");
            //kon=DriverManager.getConnection("jdbc:odbc:karina");
            kon=dbConnection.dbConnectionPath();
            Statement s= kon.createStatement();
            String sql="Select * from lakalantas where tmptkjdian = '"+this.txttmpt.getText()+"'";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                cmbsatu.setSelectedItem(r.getString("kndraanpelaku"));
                cmbdua.setSelectedItem(r.getString("kndaraankorban"));
            
                cmbjkplku.setSelectedItem(r.getString("jkpelaku"));
                cmbkrjplku.setSelectedItem(r.getString("profesipelaku"));
                cmbsimplku.setSelectedItem(r.getString("simpelaku"));
                
                
                cmbjkkorban.setSelectedItem(r.getString("jkkorban"));
                cmbkrjkorban.setSelectedItem(r.getString("profesikorban"));
                cmbsimkorban.setSelectedItem(r.getString("simkorban"));

                cmbrusak.setSelectedItem(r.getString("kerusakan"));
                cmbakibatsatu.setSelectedItem(r.getString("akibatpelaku"));
                cmbakibatdua.setSelectedItem(r.getString("akibatkorban"));
                cmbjnslaka.setSelectedItem(r.getString("jenislaka"));
                cmblokasi.setSelectedItem(r.getString("lokasi"));
            }
            r.close();
            s.close();
            //ambil_tabel_klik();
        }catch(Exception e) {
            //System.out.println("Terjadi kesalahan "+e.getMessage());
            //JOptionPane.showMessageDialog(null, "Terjadi Kesalahan");
             System.out.println(e.toString());
        }
    }
    
    
    
    private void processGetNewClusterCenter(){
        
        fz2._centerCluster.clear();
        UIKSize = 0;
        float[] ll = null; 
        try {
            kon = dbConnection.dbConnectionPath();
            Statement s = kon.createStatement();
            stat=kon.createStatement();
            
            sql = "Select * from table_PusatCluster";
            ResultSet r=s.executeQuery(sql);
            int sizenya = 0;
            if (r != null)   
            {  
                r.beforeFirst();  
                r.last();  
                sizenya = r.getRow();  
                ll = new float[16];//16 adalah jumlah field columnya
                System.out.println("hasil dapat pusat cluster : "+16);
                
                
            }
            
            stat.close();
            kon.close();
            
            kon = dbConnection.dbConnectionPath();
            s = kon.createStatement();
            stat=kon.createStatement();
            
            sql = "Select * from table_PusatCluster";
            r=s.executeQuery(sql);
            
            int i=0;
            while(r.next()){
                ll[i] = Float.parseFloat(r.getString("PusatCluster"));
                //fz2.setUIKAllReadyExist(Float.parseFloat(r.getString("PusatCluster")));
                System.out.println("Index PusatCluster : "+ll[i]);
                if(i == 15){
                    
                    fz2.setCenterClusterReadyExist(ll);
                    UIKSize += 1;//UIKSize ini hanya mengambil jumlah size aja
                    ll = new float[16];
                    i=0;
                }else{
                    i++;
                }
            }
            
            for(int a=0;a<fz2._centerCluster.size();a++){
                for(int b=0;b<fz2._centerCluster.get(a).getAtributItem().length;b++){
                    System.out.println("jumlah clusterpoinnya : "+fz2._centerCluster.get(a).getAtributItem()[b]);
                }

            }
            
            stat.close();
            kon.close();
            
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Menu_Lakalantas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Menu_Lakalantas.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    
    public void processGetReadyUIK() {
        try {
            kon = dbConnection.dbConnectionPath();
            Statement s = kon.createStatement();
            stat=kon.createStatement();
            sql = "Select * from table_UIK";
            ResultSet r=s.executeQuery(sql);
            if(r != null){
                int i=0;
                while(r.next()){
                    System.out.println("UIKSize"+UIKSize);
                    if(i != UIKSize){ //hanya di pakai untuk pembatasan aja
                        fz2.setUIKAllReadyExist(Float.parseFloat(r.getString("UIK")));
                        System.out.println("adalah"+Float.parseFloat(r.getString("UIK")));
                    }else{
                        break;
                    }
                    i++;
                }
            }
            stat.close();
            kon.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Menu_Lakalantas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Menu_Lakalantas.class.getName()).log(Level.SEVERE, null, ex);
        }
        atributLakaLantas = new AtributLakaLantas();
        float[] aa = new float[convertToAngka.size()];
        for(int a=0;a<aa.length;a++){
            aa[a]= Integer.parseInt(convertToAngka.get(a));
        }
        atributLakaLantas.setAtributItem(aa);
        fz2.setAtributLakaLantas(atributLakaLantas);
            
        model2 = new DefaultTableModel();
        this.jTable2.setModel(model2);
        for(int a=0;a<fz2._UIK.size()/fz2.Atribut.size();a++){
            model2.addColumn("UIK"+a);
        }
        
        model2.getDataVector().removeAllElements();
        model2.fireTableDataChanged();
        
        model3 = new DefaultTableModel();
                this.jTable3.setModel(model3);
                for(int a=0;a<16;a++){
                    model3.addColumn("PSC"+a);
                }
        model3.getDataVector().removeAllElements();
        model3.fireTableDataChanged();
        
        int i=0;
        while(!fz2.isFinishLoop){
            fz2.setClusterPoinKhususMenuLakalantas();
            fz2.set_totalClusterPoinKhususMenuLakalantas();
            fz2.set_Allset_totalClusterPoinKhususMenuLakalantas();
            fz2.setRegenerateUIKVersiMenuLakalantas();
            if(i==0){
                break;
            }
            i++;
        }
        fz2.perbandingan();
        
        UIK1 = new Object[fz2._UIK_VIEW.size()];
        System.out.println(fz2._UIK_VIEW.size() +"atau"+fz2.Atribut.size());
        int lengthStep = fz2._UIK_VIEW.size() / (fz2._UIK_VIEW.size() / fz2.Atribut.size());
        int stepUIK = 0;
        Object[] jTableColum = new Object[jTable2.getColumnCount()];
        TableColumn column = null;
        tableCellHasil tch = null;
        for(int z=0;z<(UIK1.length / fz2.Atribut.size());z++){
            for (int a = 0; a < lengthStep; a++) {
                tch = fz2.tandaTargetUIK.get(a);
                System.out.print("index ke : ["+(stepUIK+a)+"] : "+fz2._UIK_VIEW.get(stepUIK+a) + " , ");
                if(z==0){
                    jTableColum[z] = fz2._UIK_VIEW.get(stepUIK+a);
                    model2.addRow(jTableColum);
                   
                }else{
                    model2.setValueAt(fz2._UIK_VIEW.get(stepUIK+a), a, z);
                }
                 
                column = jTable2.getColumnModel().getColumn(z);
                UIK1[stepUIK+a] = fz2._UIK_VIEW.get(stepUIK+a);
            }
             
            column.setCellRenderer(new customCellRender(fz2.tandaTargetUIK));
            stepUIK += lengthStep;
        }
        
        
        //System.out.println("jumlah clusterpoinnya : "+jTable3.getColumnCount());*/
        Object[] jTableColum3 = new Object[jTable3.getColumnCount()];
        for(int z=0;z<UIKSize;z++){
            for(int y=0;y<jTable3.getColumnCount();y++){
                jTableColum3[y] = String.format("%.04f",(float)fz2._centerCluster.get(z).getAtributItem()[y]);
            }
            model3.addRow(jTableColum3);
        }
        
    }
    
private void ambil_data_tabel()
{
    model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            //Class.forName("sun.jdbc.odbc.JdbcOdbc");
            //kon=DriverManager.getConnection("jdbc:odbc:karina");
            kon = dbConnection.dbConnectionPath();
            Statement s= kon.createStatement();
            String sql="Select * from lakalantas";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[6];
                o[0]=r.getString("no");
                o[1]=r.getString("tgl");
                o[2]=r.getString("tmptkjdian");
                o[3]=r.getString("namapelaku");
                o[4]=r.getString("namakorban");
                o[5]=r.getString("lokasi");
                
                
                model.addRow(o);
            }
            r.close();
            s.close();
            ambil_tabel_klik();
        }catch(Exception e) {
            //System.out.println("Terjadi kesalahan "+e.getMessage());
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan");
        }
}
private void ambil_tabel_klik()
{
    int i=this.jTable1.getSelectedRow();
        
        if(i==-1)
        {
            return;
        }
        String kode = (String) model.getValueAt(i, 0);
        this.jTextField1.setText(kode);
        String tgl = (String) model.getValueAt(i, 1);
        this.txttgl.setText(tgl);
        String tmpt=(String) model.getValueAt(i, 2);
        this.txttmpt.setText(tmpt);
        this.txttmpt1.setText(tmpt);
        String nama1 = (String) model.getValueAt(i,3);
        this.txtnamaplku.setText(nama1);
        String nama2 = (String) model.getValueAt(i,4);
        this.txtnamakorban.setText(nama2);
        String lokasi = (String) model.getValueAt(i,5);
        this.cmblokasi.setSelectedItem(lokasi);
        
}
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cmbsatu = new javax.swing.JComboBox();
        txttmpt = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        cmbdua = new javax.swing.JComboBox();
        txttgl = new javax.swing.JTextField();
        txttmpt1 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtnamaplku = new javax.swing.JTextField();
        cmbjkplku = new javax.swing.JComboBox();
        cmbkrjplku = new javax.swing.JComboBox();
        cmbsimplku = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtnamakorban = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        cmbjkkorban = new javax.swing.JComboBox();
        cmbkrjkorban = new javax.swing.JComboBox();
        cmbsimkorban = new javax.swing.JComboBox();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        cmbrusak = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        cmbakibatsatu = new javax.swing.JComboBox();
        cmbakibatdua = new javax.swing.JComboBox();
        jLabel22 = new javax.swing.JLabel();
        cmbjnslaka = new javax.swing.JComboBox();
        jLabel23 = new javax.swing.JLabel();
        cmblokasi = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setText("Tanggal Kejadian");

        jLabel2.setText("Tempat Kejadian");

        jLabel3.setText("Kendaraan Yang Terlibat :");

        jLabel4.setText("1.");

        jLabel5.setText("2.");

        cmbsatu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lain-Lain", "SPM", "MoPen", "PJK", "MoBang", "" }));

        jLabel18.setText("Waktu Kejadian");

        cmbdua.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lain-Lain", "SPM", "MoPen", "PJK", "MoBang", " " }));

        jLabel26.setText("No.");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "06-09", "09-12", "12-15", "15-18", "18-21", "21-24", "24-03", "03-06" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)))
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel18)
                    .addComponent(jLabel26))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttmpt1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txttmpt)
                        .addComponent(cmbsatu, 0, 109, Short.MAX_VALUE)
                        .addComponent(cmbdua, 0, 109, Short.MAX_VALUE)
                        .addComponent(txttgl))
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel26))
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txttgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(txttmpt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(cmbsatu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cmbdua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttmpt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("IDENTITAS PELAKU");

        jLabel7.setText("Nama");

        jLabel8.setText("JK");

        jLabel9.setText("Profesi");

        jLabel10.setText("SIM");

        jLabel11.setText("Usia");

        cmbjkplku.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "Laki-Laki", "Perempuan" }));

        cmbkrjplku.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lain-Lain", "PNS", "Swasta", "Mahasiswa", "Pelajar", "Pengemudi", "TNI/POLRI", " ", " " }));

        cmbsimplku.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tanpa SIM", "A", "A UMUM", "BI", "BI UMUM", "BII ", "BII UMUM", "C", " " }));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "10-15", "16-30", "31-40", "41-50", ">50", " " }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbjkplku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtnamaplku, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbkrjplku, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbsimplku, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtnamaplku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(cmbjkplku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cmbkrjplku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cmbsimplku, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("IDENTITAS KORBAN");

        jLabel13.setText("Nama");

        jLabel14.setText("JK");

        jLabel15.setText("Profesi");

        jLabel16.setText("SIM");

        jLabel17.setText("Usia");

        cmbjkkorban.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "Laki-Laki", "Perempuan" }));

        cmbkrjkorban.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lain-Lain", "PNS", "Swasta", "Mahasiswa", "Pelajar", "Pengemudi", "TNI/POLRI" }));
        cmbkrjkorban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbkrjkorbanActionPerformed(evt);
            }
        });

        cmbsimkorban.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tanpa SIM", "A", "A UMUM", "BI", "BI UMUM", "BII ", "BII UMUM", "C" }));

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "10-15", "16-30", "31-40", "41-50", ">50", " " }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14))
                        .addGap(57, 57, 57)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnamakorban, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbjkkorban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addGap(51, 51, 51)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbsimkorban, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbkrjkorban, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel12))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtnamakorban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(cmbjkkorban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(cmbkrjkorban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(cmbsimkorban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jLabel19.setText("Kerusakan Kendaraan");

        cmbrusak.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "Rusak Ringan", "Rusak Berat", "Rusak Sedang" }));

        jLabel20.setText("Akibat Kejadian Pelaku");

        jLabel21.setText("Akibat Kejadian Korban");

        cmbakibatsatu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "Luka Ringan", "Luka Berat", "Meninggal Dunia" }));

        cmbakibatdua.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nihil", "Luka Ringan", "Luka Berat", "Meninggal Dunia" }));

        jLabel22.setText("Jenis Kecelakaan");

        cmbjnslaka.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lain-Lain", "Tunggal", "DPN-DPN", "DPN-BLK", "DPN-SMP", "SMP-SMP", "SMP-BLK", "Hewan", " " }));

        jLabel23.setText("Lokasi Kejadian");

        cmblokasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lain-Lain", "Jalan Raya", "Pertokoan", "Perbelanjaan", "Pemukiman", "Wisata", "Tempat Hiburan", "Perkantoran" }));

        jButton1.setText("Baru");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Simpan");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Ubah");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Hapus");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel24.setText("MASTER DATA LAKA LANTAS");

        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseClicked(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jTabbedPane1.addTab("Data Kecelakaan", jScrollPane1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jTabbedPane1.addTab("UIK", jScrollPane2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jTabbedPane1.addTab("Pusat Cluster", jScrollPane3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel20)
                                    .addComponent(jLabel21)
                                    .addComponent(jLabel22)
                                    .addComponent(jLabel23))
                                .addGap(8, 8, 8)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cmblokasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbjnslaka, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbakibatdua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbakibatsatu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbrusak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton4)))))
                .addGap(109, 109, 109))
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jLabel25)
                .addGap(70, 70, 70)
                .addComponent(jLabel24)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabel25)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel24)
                        .addGap(30, 30, 30)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(cmbrusak, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(cmbakibatsatu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbakibatdua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(cmbjnslaka, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(cmblokasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2)
                            .addComponent(jButton3)
                            .addComponent(jButton4)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel25.getAccessibleContext().setAccessibleName("jLabel25");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
// TODO add your handling code here:
         if(txttgl.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txttmpt.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txtnamaplku.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txtnamakorban.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else{
            
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbc");
            //kon=DriverManager.getConnection("jdbc:odbc:karina");
            kon=dbConnection.dbConnectionPath();
            sql="UPDATE lakalantas " +
                    "SET tgl='"+ txttgl.getText() +"', tmptkjdian='"+txttmpt.getText()+"', " +
                    "kndraanpelaku='"+ cmbsatu.getSelectedItem() +"',kndaraankorban='"+cmbdua.getSelectedItem()+"'," +
                    "waktu='"+jComboBox1.getSelectedItem()+"',namapelaku='"+txtnamaplku.getText()+"',jkpelaku='"+cmbjkplku.getSelectedItem()+"'," +
                    "profesipelaku='"+cmbkrjplku.getSelectedItem()+"',simpelaku='"+cmbsimplku.getSelectedItem()+"'," +
                    "usiapelaku='"+jComboBox2.getSelectedItem()+"',namakorban='"+txtnamakorban.getText()+"'," +
                    "jkkorban='"+cmbjkkorban.getSelectedItem()+"',profesikorban='"+cmbkrjkorban.getSelectedItem()+"'," +
                    "simkorban='"+cmbsimplku.getSelectedItem()+"',usiakorban='"+jComboBox3.getSelectedItem()+"',k" +
                    "erusakan='"+cmbrusak.getSelectedItem()+"',akibatpelaku='"+cmbakibatsatu.getSelectedItem()+"'," +
                    "akibatkorban='"+cmbakibatdua.getSelectedItem()+"',jenislaka='"+cmbjnslaka.getSelectedItem()+"',lokasi='"+cmblokasi.getSelectedItem()+"'" +
                    " WHERE no = '"+jTextField1.getText()+"'";
            //sql="insert into lakalantas(tmptkjdian,kndraanpelaku,kndraankorban,waktu,namapelaku,jkpelaku,profesipelaku,simpelaku,usiapelaku,namakorban,jkkorban,profesikorban,simkorban,usiakorban,kerusakan,akibatpelaku,akibatkorban,jenislaka,lokasi) values('"+txttmpt.getText()+"','"+cmbsatu.getSelectedItem()+"','"+cmbdua.getSelectedItem()+"','"+txtwaktu.getText()+"','"+txtnamaplku.getText()+"','"+cmbjkplku.getSelectedItem()+"','"+cmbkrjplku.getSelectedItem()+"','"+cmbsimplku.getSelectedItem()+"','"+txtumurplku.getText()+"','"+txtnamakorban.getText()+"','"+cmbjkkorban.getSelectedItem()+"','"+cmbkrjkorban.getSelectedItem()+"','"+cmbsimkorban.getSelectedItem()+"','"+txtumurkorban.getText()+"','"+cmbrusak.getSelectedItem()+"','"+cmbakibatsatu.getSelectedItem()+"','"+cmbakibatdua.getSelectedItem()+"','"+cmbjnslaka.getSelectedItem()+"','"+cmblokasi.getSelectedItem()+"')";
            stat=kon.createStatement();
            stat.execute(sql);
            ambil_data_tabel();
            kosong();
            JOptionPane.showMessageDialog(null, "Data Diubah");
        } catch (Exception e) {
        }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
// TODO add your handling code here:
        if(txttgl.equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txttmpt.equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txtnamaplku.equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txtnamakorban.equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else{
            
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbc");
            //kon=DriverManager.getConnection("jdbc:odbc:karina");
            kon=dbConnection.dbConnectionPath();
            sql="DELETE FROM lakalantas WHERE tmptkjdian = '"+txttmpt.getText()+"'";
            //sql="insert into lakalantas(tmptkjdian,kndraanpelaku,kndraankorban,waktu,namapelaku,jkpelaku,profesipelaku,simpelaku,usiapelaku,namakorban,jkkorban,profesikorban,simkorban,usiakorban,kerusakan,akibatpelaku,akibatkorban,jenislaka,lokasi) values('"+txttmpt.getText()+"','"+cmbsatu.getSelectedItem()+"','"+cmbdua.getSelectedItem()+"','"+txtwaktu.getText()+"','"+txtnamaplku.getText()+"','"+cmbjkplku.getSelectedItem()+"','"+cmbkrjplku.getSelectedItem()+"','"+cmbsimplku.getSelectedItem()+"','"+txtumurplku.getText()+"','"+txtnamakorban.getText()+"','"+cmbjkkorban.getSelectedItem()+"','"+cmbkrjkorban.getSelectedItem()+"','"+cmbsimkorban.getSelectedItem()+"','"+txtumurkorban.getText()+"','"+cmbrusak.getSelectedItem()+"','"+cmbakibatsatu.getSelectedItem()+"','"+cmbakibatdua.getSelectedItem()+"','"+cmbjnslaka.getSelectedItem()+"','"+cmblokasi.getSelectedItem()+"')";
            stat=kon.createStatement();
            stat.execute(sql);
            ambil_data_tabel();
            kosong();
            JOptionPane.showMessageDialog(null, "Data Dihapus");
        } catch (Exception e) {
        }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
// TODO add your handling code here:
         this.ambil_tabel_klik();
         caridata();
         aktif();
    }//GEN-LAST:event_jTable1MouseClicked

    private void jScrollPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseClicked
// TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane1MouseClicked
   
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
// TODO add your handling code here:
        if(txttgl.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txttmpt.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txtnamaplku.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else if(txtnamakorban.getText().equals("")){
             JOptionPane.showMessageDialog(null, "Data kosong");
        }else{
            convertToAngka.clear();
            convertToAngka.add(cmbsatu.getSelectedItem().toString());
            convertToAngka.add(cmbdua.getSelectedItem().toString());
            convertToAngka.add(jComboBox1.getSelectedItem().toString());
            convertToAngka.add(cmbjkplku.getSelectedItem().toString());
            convertToAngka.add(cmbkrjplku.getSelectedItem().toString());
            convertToAngka.add(cmbsimplku.getSelectedItem().toString());
            convertToAngka.add(jComboBox2.getSelectedItem().toString());
            convertToAngka.add(cmbjkkorban.getSelectedItem().toString());
            convertToAngka.add(cmbkrjkorban.getSelectedItem().toString());
            convertToAngka.add(cmbsimkorban.getSelectedItem().toString());
            convertToAngka.add(jComboBox3.getSelectedItem().toString());
            convertToAngka.add(cmbrusak.getSelectedItem().toString());
            convertToAngka.add(cmbakibatsatu.getSelectedItem().toString());
            convertToAngka.add(cmbakibatdua.getSelectedItem().toString());
            convertToAngka.add(cmbjnslaka.getSelectedItem().toString());
            convertToAngka.add(cmblokasi.getSelectedItem().toString());
            
            for(int a=0;a<convertToAngka.size();a++){
                if (convertToAngka.get(a).equals("SPM")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("MoPen")) {
                   convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("MoBang")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("PJK")) {
                    convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("Lain-Lain")) {
                    convertToAngka.set(a, String.valueOf(9));
                } else if (convertToAngka.get(a).equals("Laki-Laki")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("Perempuan")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("PNS")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("TNI/POLRI")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("Mahasiswa")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("Pelajar")) {
                    convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("Pengemudi")) {
                    convertToAngka.set(a, String.valueOf(5));
                } else if (convertToAngka.get(a).equals("Swasta")) {
                    convertToAngka.set(a, String.valueOf(6));
                } else if (convertToAngka.get(a).equals("A")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("A UMUM")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("BI")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("BI UM")) {
                    convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("BII")) {
                    convertToAngka.set(a, String.valueOf(5));
                } else if (convertToAngka.get(a).equals("BII U")) {
                    convertToAngka.set(a, String.valueOf(6));
                } else if (convertToAngka.get(a).equals("C")) {
                    convertToAngka.set(a, String.valueOf(7));
                } else if (convertToAngka.get(a).equals("Tanpa")) {
                    convertToAngka.set(a, String.valueOf(8));
                } else if (convertToAngka.get(a).equals("Rusak Ringan")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("Rusak Berat")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("Meninggal Dunia")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("Luka Berat")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("Luka Ringan")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("Tunggal")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("DPN-DPN")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("DPN-BLK")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("DPN-SMP")) {
                    convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("SMP-SMP")) {
                    convertToAngka.set(a, String.valueOf(5));
                } else if (convertToAngka.get(a).equals("SMP-BLK")) {
                    convertToAngka.set(a, String.valueOf(6));
                } else if (convertToAngka.get(a).equals("Hewan")) {
                    convertToAngka.set(a, String.valueOf(7));
                } else if (convertToAngka.get(a).equals("Pemukiman")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("Pertokoan")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("Perbelanjaan")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("Wisata")) {
                    convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("Tempat Hiburan")) {
                    convertToAngka.set(a, String.valueOf(5));
                } else if (convertToAngka.get(a).equals("Jalan Raya")) {
                    convertToAngka.set(a, String.valueOf(6));
                } else if (convertToAngka.get(a).equals("Perkantoran")) {
                    convertToAngka.set(a, String.valueOf(7));
                } else if (convertToAngka.get(a).equals("Nihil")) {
                    convertToAngka.set(a, String.valueOf(8));
                } else if (convertToAngka.get(a).equals("Lain-lain")) {
                    convertToAngka.set(a, String.valueOf(9));
                } else if (convertToAngka.get(a).equals("06-09")) {
                    convertToAngka.set(a, String.valueOf(10));
                } else if (convertToAngka.get(a).equals("09-12")) {
                    convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("12-15")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("15-18")) {
                   convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("18-21")) {
                    convertToAngka.set(a, String.valueOf(5));
                } else if (convertToAngka.get(a).equals("21-24")) {
                    convertToAngka.set(a, String.valueOf(6));
                } else if (convertToAngka.get(a).equals("24-03")) {
                    convertToAngka.set(a, String.valueOf(7));
                } else if (convertToAngka.get(a).equals("03-06")) {
                    convertToAngka.set(a, String.valueOf(8));
                } else if (convertToAngka.get(a).equals("10-15")) {
                    convertToAngka.set(a, String.valueOf(1));
                } else if (convertToAngka.get(a).equals("16-30")) {
                   convertToAngka.set(a, String.valueOf(2));
                } else if (convertToAngka.get(a).equals("31-40")) {
                    convertToAngka.set(a, String.valueOf(3));
                } else if (convertToAngka.get(a).equals("41-50")) {
                    convertToAngka.set(a, String.valueOf(4));
                } else if (convertToAngka.get(a).equals("<50")) {
                    convertToAngka.set(a, String.valueOf(5));
                }
            }
        try {
            //Class.forName("sun.jdbc.odbc.JdbcOdbc");
            //kon=DriverManager.getConnection("jdbc:odbc:karina");
            kon=dbConnection.dbConnectionPath();
            sql="insert into lakalantas(tgl,tmptkjdian,kndraanpelaku,kndaraankorban,waktu,namapelaku,jkpelaku,profesipelaku,simpelaku,usiapelaku,namakorban,jkkorban,profesikorban,simkorban,usiakorban,kerusakan,akibatpelaku,akibatkorban,jenislaka,lokasi) " +
                    "values ('"+txttgl.getText()+"','"+txttmpt.getText()+"','"+cmbsatu.getSelectedItem()+"','"+cmbdua.getSelectedItem()+"','"+jComboBox1.getSelectedItem()+"','"+txtnamaplku.getText()+"','"+cmbjkplku.getSelectedItem()+"','"+cmbkrjplku.getSelectedItem()+"','"+cmbsimplku.getSelectedItem()+"','"+jComboBox2.getSelectedItem()+"','"+txtnamakorban.getText()+"','"+cmbjkkorban.getSelectedItem()+"','"+cmbkrjkorban.getSelectedItem()+"','"+cmbsimkorban.getSelectedItem()+"','"+jComboBox3.getSelectedItem()+"','"+cmbrusak.getSelectedItem()+"','"+cmbakibatsatu.getSelectedItem()+"','"+cmbakibatdua.getSelectedItem()+"','"+cmbjnslaka.getSelectedItem()+"','"+cmblokasi.getSelectedItem()+"')";
            stat=kon.createStatement();
            stat.execute(sql);
            ambil_data_tabel();
            kosong();
            JOptionPane.showMessageDialog(null, "Data Disimpan");
        } catch (Exception e) {
        }
        }
        
        processGetNewClusterCenter();
        processGetReadyUIK();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
// TODO add your handling code here:
        fz2 = new fuzzyLogicKhususMenuLakalantas();
        fz2.resetTempDB();
        aktif();
    }//GEN-LAST:event_jButton1ActionPerformed
    private void aktif(){
        txttgl.enable(true);
        txttmpt.enable(true);
        cmbsatu.enable(true);
        cmbdua.enable(true);

        txtnamaplku.enable(true);
        cmbjkplku.enable(true);
        cmbkrjplku.enable(true);
        cmbsimplku.enable(true);

        txtnamakorban.enable(true);
        cmbjkkorban.enable(true);
        cmbkrjkorban.enable(true);
        cmbsimkorban.enable(true);

        cmbrusak.enable(true);
        cmbakibatsatu.enable(true);
        cmbakibatdua.enable(true);
        cmbjnslaka.enable(true);
        cmblokasi.enable(true);
        jButton2.enable(true);
        jButton3.enable(true);
        jButton4.enable(true);
    }
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
// TODO add your handling code here:
        mati();
        ambil_data_tabel();
        txttmpt1.setVisible(false);
    }//GEN-LAST:event_formWindowOpened
    private void mati(){
        txttgl.enable(false);
        txttmpt.enable(false);
        cmbsatu.enable(false);
        cmbdua.enable(false);

        txtnamaplku.enable(false);
        cmbjkplku.enable(false);
        cmbkrjplku.enable(false);
        cmbsimplku.enable(false);

        txtnamakorban.enable(false);
        cmbjkkorban.enable(false);
        cmbkrjkorban.enable(false);
        cmbsimkorban.enable(false);

        cmbrusak.enable(false);
        cmbakibatsatu.enable(false);
        cmbakibatdua.enable(false);
        cmbjnslaka.enable(false);
        cmblokasi.enable(false);
        jButton2.enable(false);
        jButton3.enable(false);
        jButton4.enable(false);
    }
    private void kosong(){
        txttmpt.setText("");
        txtnamaplku.setText("");
        txtnamakorban.setText("");
    }
    private void cmbkrjkorbanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbkrjkorbanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbkrjkorbanActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2ActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu_Lakalantas().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbakibatdua;
    private javax.swing.JComboBox cmbakibatsatu;
    private javax.swing.JComboBox cmbdua;
    private javax.swing.JComboBox cmbjkkorban;
    private javax.swing.JComboBox cmbjkplku;
    private javax.swing.JComboBox cmbjnslaka;
    private javax.swing.JComboBox cmbkrjkorban;
    private javax.swing.JComboBox cmbkrjplku;
    private javax.swing.JComboBox cmblokasi;
    private javax.swing.JComboBox cmbrusak;
    private javax.swing.JComboBox cmbsatu;
    private javax.swing.JComboBox cmbsimkorban;
    private javax.swing.JComboBox cmbsimplku;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField txtnamakorban;
    private javax.swing.JTextField txtnamaplku;
    private javax.swing.JTextField txttgl;
    private javax.swing.JTextField txttmpt;
    private javax.swing.JTextField txttmpt1;
    // End of variables declaration//GEN-END:variables
    
}
