/*
 * Menu_MasterLakalantas.java
 *
 * Created on September 5, 2013, 12:30 PM
 */

package skripsikarina;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import skripsikarina.helper.dbConnection;
/**
 *
 * @author  karin
 */
public class Menu_MasterLakalantas extends javax.swing.JFrame {
    public Connection kon;
    public Statement stat;
    public ResultSet rs;
    public String sql="";
    private DefaultTableModel model;
    /** Creates new form Menu_MasterLakalantas */
    public Menu_MasterLakalantas() {
        initComponents();
        model=new DefaultTableModel();
        this.jTable2.setModel(model);
        model.addColumn("Tgl");
        model.addColumn("Tempat");
        model.addColumn("Kendaraan Pelaku");
        model.addColumn("Kendaraan Korban");
                model.addColumn("Waktu");
        model.addColumn("Nama Pelaku");
                model.addColumn("SIM");
        model.addColumn("Nama Korban");
                model.addColumn("SIM");
        model.addColumn("Kerusakan");
        model.addColumn("Akibat Pelaku");
        model.addColumn("Akibat Korban");
                model.addColumn("Jenis Laka");
        model.addColumn("Lokasi");

        ambil_data_tabel();
    }
    private void ambil_data_tabel()
{
    model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        try {            
            //Class.forName("com.mysql.jdbc.Driver");//sun.jdbc.odbc.JdbcOdbc          
            //kon=DriverManager.getConnection("jdbc:mysql://localhost:3306/karina","root", "");//jdbc:odbc:karina
            
            kon=dbConnection.dbConnectionPath();
            Statement s= kon.createStatement();
            String sql="Select * from lakalantas";
            ResultSet r=s.executeQuery(sql);
            
            while (r.next()) {
                Object[] o=new Object[14];
                o[0]=r.getString("tgl");
                o[1]=r.getString("tmptkjdian");
                o[2]=r.getString("kndraanpelaku");
                o[3]=r.getString("kndaraankorban");
                o[4]=r.getString("waktu");
                o[5]=r.getString("namapelaku");
                o[6]=r.getString("simpelaku");
                o[7]=r.getString("namakorban");
                o[8]=r.getString("simkorban");
                o[9]=r.getString("kerusakan");
                o[10]=r.getString("akibatpelaku");
                o[11]=r.getString("akibatkorban");
                o[12]=r.getString("jenislaka");
                o[13]=r.getString("lokasi");
                model.addRow(o);
            }
            r.close();
            s.close();
            //ambil_tabel_klik();
        }catch(Exception e) {
            //System.out.println("Terjadi kesalahan "+e.getMessage());
            //JOptionPane.showMessageDialog(null, e);
             System.out.println(e.toString());
        }
}
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Master Data Laka Lantas");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(253, 253, 253)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 965, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
// TODO add your handling code here:
    }//GEN-LAST:event_formWindowOpened
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu_MasterLakalantas().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable2;
    // End of variables declaration//GEN-END:variables
    
}
