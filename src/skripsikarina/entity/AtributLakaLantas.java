/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skripsikarina.entity;

/**
 *
 * @author donny
 */
public class AtributLakaLantas {
    
    private Double KendaraanPelaku;
    private Double kendaraanKorban;
    private Double waktu;
    private Double JKPelaku;
    private Double ProfesiPelaku;
    private Double SimPelaku;
    private Double UsiaPelaku;
    private Double JkKorban;
    private Double ProfesiKorban;
    private Double SimKorban;
    private Double UsiaKorban;
    private Double Kerusakan;
    private Double AkibatPelaku;
    private Double AkibatKorban;
    private Double JenisLaka;
    private Double Lokasi;
    
    private float[] atributItem = new float[16];
    
    /**
     * @return the KendaraanPelaku
     */
    public Double getKendaraanPelaku() {
        return KendaraanPelaku;
    }

    /**
     * @param KendaraanPelaku the KendaraanPelaku to set
     */
    public void setKendaraanPelaku(Double KendaraanPelaku) {
        this.KendaraanPelaku = KendaraanPelaku;
    }

    /**
     * @return the kendaraanKorban
     */
    public Double getKendaraanKorban() {
        return kendaraanKorban;
    }

    /**
     * @param kendaraanKorban the kendaraanKorban to set
     */
    public void setKendaraanKorban(Double kendaraanKorban) {
        this.kendaraanKorban = kendaraanKorban;
    }

    /**
     * @return the waktu
     */
    public Double getWaktu() {
        return waktu;
    }

    /**
     * @param waktu the waktu to set
     */
    public void setWaktu(Double waktu) {
        this.waktu = waktu;
    }

    /**
     * @return the JKPelaku
     */
    public Double getJKPelaku() {
        return JKPelaku;
    }

    /**
     * @param JKPelaku the JKPelaku to set
     */
    public void setJKPelaku(Double JKPelaku) {
        this.JKPelaku = JKPelaku;
    }

    /**
     * @return the ProfesiPelaku
     */
    public Double getProfesiPelaku() {
        return ProfesiPelaku;
    }

    /**
     * @param ProfesiPelaku the ProfesiPelaku to set
     */
    public void setProfesiPelaku(Double ProfesiPelaku) {
        this.ProfesiPelaku = ProfesiPelaku;
    }

    /**
     * @return the SimPelaku
     */
    public Double getSimPelaku() {
        return SimPelaku;
    }

    /**
     * @param SimPelaku the SimPelaku to set
     */
    public void setSimPelaku(Double SimPelaku) {
        this.SimPelaku = SimPelaku;
    }

    /**
     * @return the UsiaPelaku
     */
    public Double getUsiaPelaku() {
        return UsiaPelaku;
    }

    /**
     * @param UsiaPelaku the UsiaPelaku to set
     */
    public void setUsiaPelaku(Double UsiaPelaku) {
        this.UsiaPelaku = UsiaPelaku;
    }


    /**
     * @return the JkKorban
     */
    public Double getJkKorban() {
        return JkKorban;
    }

    /**
     * @param JkKorban the JkKorban to set
     */
    public void setJkKorban(Double JkKorban) {
        this.JkKorban = JkKorban;
    }

    /**
     * @return the ProfesiKorban
     */
    public Double getProfesiKorban() {
        return ProfesiKorban;
    }

    /**
     * @param ProfesiKorban the ProfesiKorban to set
     */
    public void setProfesiKorban(Double ProfesiKorban) {
        this.ProfesiKorban = ProfesiKorban;
    }

    /**
     * @return the SimKorban
     */
    public Double getSimKorban() {
        return SimKorban;
    }

    /**
     * @param SimKorban the SimKorban to set
     */
    public void setSimKorban(Double SimKorban) {
        this.SimKorban = SimKorban;
    }

    /**
     * @return the UsiaKorban
     */
    public Double getUsiaKorban() {
        return UsiaKorban;
    }

    /**
     * @param UsiaKorban the UsiaKorban to set
     */
    public void setUsiaKorban(Double UsiaKorban) {
        this.UsiaKorban = UsiaKorban;
    }

    /**
     * @return the Kerusakan
     */
    public Double getKerusakan() {
        return Kerusakan;
    }

    /**
     * @param Kerusakan the Kerusakan to set
     */
    public void setKerusakan(Double Kerusakan) {
        this.Kerusakan = Kerusakan;
    }

    /**
     * @return the AkibatPelaku
     */
    public Double getAkibatPelaku() {
        return AkibatPelaku;
    }

    /**
     * @param AkibatPelaku the AkibatPelaku to set
     */
    public void setAkibatPelaku(Double AkibatPelaku) {
        this.AkibatPelaku = AkibatPelaku;
    }

    /**
     * @return the AkibatKorban
     */
    public Double getAkibatKorban() {
        return AkibatKorban;
    }

    /**
     * @param AkibatKorban the AkibatKorban to set
     */
    public void setAkibatKorban(Double AkibatKorban) {
        this.AkibatKorban = AkibatKorban;
    }

    /**
     * @return the JenisLaka
     */
    public Double getJenisLaka() {
        return JenisLaka;
    }

    /**
     * @param JenisLaka the JenisLaka to set
     */
    public void setJenisLaka(Double JenisLaka) {
        this.JenisLaka = JenisLaka;
    }

    /**
     * @return the Lokasi
     */
    public Double getLokasi() {
        return Lokasi;
    }

    /**
     * @param Lokasi the Lokasi to set
     */
    public void setLokasi(Double Lokasi) {
        this.Lokasi = Lokasi;
    }

    /**
     * @return the atributItem
     */
    public float[] getAtributItem() {
        return atributItem;
    }

    /**
     * @param atributItem the atributItem to set
     */
    public void setAtributItem(float[] atributItem) {
        this.atributItem = atributItem;
    }

    
    
    
}
