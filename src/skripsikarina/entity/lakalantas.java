/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skripsikarina.entity;

import java.util.Date;

/**
 *
 * @author donny
 */
public class lakalantas {
    private long no;
    private Date date;
    private String TempatKejadian;
    private String KendaraanPelaku;
    private String kendaraanKorban;
    private String waktu;
    private String NamaPelaku;
    private String JKPelaku;
    private String ProfesiPelaku;
    private String SimPelaku;
    private String UsiaPelaku;
    private String NamaKorban;
    private String JkKorban;
    private String ProfesiKorban;
    private String SimKorban;
    private String UsiaKorban;
    private String Kerusakan;
    private String AkibatPelaku;
    private String AkibatKorban;
    private String JenisLaka;
    private String Lokasi;

    public long getNo() {
        return no;
    }

    public void setNo(long no) {
        this.no = no;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTempatKejadian() {
        return TempatKejadian;
    }

    public void setTempatKejadian(String TempatKejadian) {
        this.TempatKejadian = TempatKejadian;
    }

    public String getKendaraanPelaku() {
        return KendaraanPelaku;
    }

    public void setKendaraanPelaku(String KendaraanPelaku) {
        this.KendaraanPelaku = KendaraanPelaku;
    }

    public String getKendaraanKorban() {
        return kendaraanKorban;
    }

    public void setKendaraanKorban(String kendaraanKorban) {
        this.kendaraanKorban = kendaraanKorban;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getNamaPelaku() {
        return NamaPelaku;
    }

    public void setNamaPelaku(String NamaPelaku) {
        this.NamaPelaku = NamaPelaku;
    }

    public String getJKPelaku() {
        return JKPelaku;
    }

    public void setJKPelaku(String JKPelaku) {
        this.JKPelaku = JKPelaku;
    }

    public String getProfesiPelaku() {
        return ProfesiPelaku;
    }

    public void setProfesiPelaku(String ProfesiPelaku) {
        this.ProfesiPelaku = ProfesiPelaku;
    }

    public String getSimPelaku() {
        return SimPelaku;
    }

    public void setSimPelaku(String SimPelaku) {
        this.SimPelaku = SimPelaku;
    }

    public String getUsiaPelaku() {
        return UsiaPelaku;
    }

    public void setUsiaPelaku(String UsiaPelaku) {
        this.UsiaPelaku = UsiaPelaku;
    }

    public String getNamaKorban() {
        return NamaKorban;
    }

    public void setNamaKorban(String NamaKorban) {
        this.NamaKorban = NamaKorban;
    }

    public String getJkKorban() {
        return JkKorban;
    }

    public void setJkKorban(String JkKorban) {
        this.JkKorban = JkKorban;
    }

    public String getProfesiKorban() {
        return ProfesiKorban;
    }

    public void setProfesiKorban(String ProfesiKorban) {
        this.ProfesiKorban = ProfesiKorban;
    }

    public String getSimKorban() {
        return SimKorban;
    }

    public void setSimKorban(String SimKorban) {
        this.SimKorban = SimKorban;
    }

    public String getUsiaKorban() {
        return UsiaKorban;
    }

    public void setUsiaKorban(String UsiaKorban) {
        this.UsiaKorban = UsiaKorban;
    }

    public String getKerusakan() {
        return Kerusakan;
    }

    public void setKerusakan(String Kerusakan) {
        this.Kerusakan = Kerusakan;
    }

    public String getAkibatPelaku() {
        return AkibatPelaku;
    }

    public void setAkibatPelaku(String AkibatPelaku) {
        this.AkibatPelaku = AkibatPelaku;
    }

    public String getAkibatKorban() {
        return AkibatKorban;
    }

    public void setAkibatKorban(String AkibatKorban) {
        this.AkibatKorban = AkibatKorban;
    }

    public String getJenisLaka() {
        return JenisLaka;
    }

    public void setJenisLaka(String JenisLaka) {
        this.JenisLaka = JenisLaka;
    }

    public String getLokasi() {
        return Lokasi;
    }

    public void setLokasi(String Lokasi) {
        this.Lokasi = Lokasi;
    }
    
    
    
}
