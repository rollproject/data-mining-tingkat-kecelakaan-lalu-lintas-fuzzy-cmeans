/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skripsikarina.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import skripsikarina.entity.AtributLakaLantas;
import skripsikarina.entity.tableCellHasil;

/**
 *
 * @author donny
 */
public class fuzzyLogicKhususMenuLakalantas {

    //STEP 1
    public ArrayList<Float> _UIK = new ArrayList<Float>();
    public ArrayList<Float> _UIK_VIEW = new ArrayList<Float>();
    public ArrayList<Float> tempDesckUIK = new ArrayList<Float>();
    public ArrayList<AtributLakaLantas> Atribut = new ArrayList<AtributLakaLantas>();
    public int SizeRootStep, SizeSubStep;

    //STEP 2
    public ArrayList<Float> ResultUIK = new ArrayList<Float>();
    public ArrayList<AtributLakaLantas> _resultAtribut = new ArrayList<AtributLakaLantas>();
    public ArrayList<AtributLakaLantas> _centerCluster = new ArrayList<AtributLakaLantas>();

    //STEP 3
    public ArrayList<AtributLakaLantas> _centerClusterUIK = new ArrayList<AtributLakaLantas>();
    public ArrayList<AtributLakaLantas> _clusterPoin = new ArrayList<AtributLakaLantas>();
    public ArrayList<Float> _totalClusterPoin = new ArrayList<Float>();
    public ArrayList<Float> _totalAllClusterPoin = new ArrayList<Float>();
    public ArrayList<Integer> _indexKhususUIK = new ArrayList<Integer>();
    public ArrayList<tableCellHasil> tandaTargetUIK = new ArrayList<tableCellHasil>();

    //variable enggak dipake
    public ArrayList<Double> _resultQuadratUIK = new ArrayList<Double>();
    public ArrayList<Double> _resultClusterUIK = new ArrayList<Double>();
    public ArrayList<Double> _resultTotalCluster = new ArrayList<Double>();
    public int pembanding1=0,pembanding2=0;
    public boolean isFinishLoop = false;
    
    //pangkat
    private double setPangkat(int size, double value) {
        double resultValue = 0.0;
        for (int a = 0; a < size; a++) {
            if (a == 0) {
                resultValue = value;
            } else {
                resultValue *= value;
            }
        }
        return resultValue;
    }

    //PROCESS STEP 1
    public void setUIK(int setjmlUIK) {
        //if (!_UIK.isEmpty()) {
            _UIK.clear();
        //}

        //INGAT DON HANYA TESTING
        
        
        
        for(int a=0;a<setjmlUIK;a++){
            /*if(a % 2==0){
                for (int z = 0; z < Atribut.size(); z++) {
                    /*if(z %2==0){
                       _UIK.add((float)0); 
                    }else{
                       _UIK.add((float)1); 
                    }*/
                    // _UIK.add(Float.parseFloat(String.valueOf(String.format("%.05f", RandomUIK.getRandomUIK(0.1, 1))).replace(",", ".")));
                    //_UIK.add((float)0.4);

               // }
            //}else{
                for (int z = 0; z < Atribut.size(); z++) {
                    _UIK.add(Float.parseFloat(String.valueOf(String.format("%.09f", RandomUIK.getRandomUIK(0.1, 1))).replace(",", ".")));
                    //_UIK.add((float)0.5);
                    /*if(z %2==0){
                       _UIK.add((float)1); 
                    }else{
                       _UIK.add((float)0); 
                    }*/
                }
            //}
        }

       /*for (int z = 0; z < Atribut.size(); z++) {
            _UIK.add(Float.parseFloat(String.valueOf(String.format("%.01f", RandomUIK.getRandomUIK(0.1, 1))).replace(",", ".")));
            //_UIK.add((float)0.2);

        }*/
      
        
        SizeRootStep = (_UIK.size() / Atribut.size());
        SizeSubStep = (_UIK.size() / (_UIK.size() / Atribut.size()));

    }
    
    //ini khusus punya bagian Menu_InfoLakalantas
    public void setUIKAllReadyExist(float setjmlUIK) {
        _UIK.add(Float.parseFloat(String.valueOf(String.format("%.09f", setjmlUIK))));
    }

    public void setAtributLakaLantas(AtributLakaLantas tempArray) {
        Atribut.add(tempArray);
    }

    //END PROCESS STEP1
    //PROCESS STEP 2
    public void setResultUIK() {
        for (int a = 0; a < _UIK.size(); a++) {
            ResultUIK.add(_UIK.get(a) * _UIK.get(a));
        }
        System.out.println();
        for (int a = 0; a < _UIK.size(); a++) {
            System.out.println("huhu : " + ResultUIK.get(a) + " dengan : " + _UIK.get(a));
        }

    }

    public void setResultAtribut() {
        int divUIK = ResultUIK.size() / Atribut.size();
        int stepUIK = 0;
        AtributLakaLantas atr;

        float[] items_atribut;// = new double[Atribut.get(0).getAtributItem().length]; //mengikuti length row atribut
        
        for (int div = 0; div < divUIK; div++) {
            for (int a = 0; a < Atribut.size(); a++) {
                items_atribut = new float[Atribut.get(0).getAtributItem().length];
                for (int b = 0; b < Atribut.get(a).getAtributItem().length; b++) {
                    //System.out.println(stepUIK+b);

                    items_atribut[b] = Atribut.get(a).getAtributItem()[b] * ResultUIK.get(stepUIK + a);

                    System.out.println("putaran ke div : "+div+" => "+(stepUIK + a) + " adalah : " + ResultUIK.get(stepUIK + a)+" dan untuk atributnya adalah :"+Atribut.get(a).getAtributItem()[b]+" hasilnya : "+(Atribut.get(a).getAtributItem()[b]*ResultUIK.get(stepUIK + a)));
                    //break;
                }
                atr = new AtributLakaLantas();
                atr.setAtributItem(items_atribut);
                _resultAtribut.add(atr);

            }
            stepUIK += ResultUIK.size() / divUIK;
            //break;
        }
        
        for (int a = 0; a < _resultAtribut.size(); a++) {
            for(int b=0;b<_resultAtribut.get(a).getAtributItem().length;b++){
                System.out.println("_resultAtribut ke : " + a+" "+_resultAtribut.get(a).getAtributItem()[b]);
            }
            System.out.println("dan");
        }
        
    }

   
    //ini khusus bagian Menu_Lakalantas class
    public void setCenterClusterReadyExist(float[] centerClusternya) {
        AtributLakaLantas atr = new AtributLakaLantas();
        atr.setAtributItem(centerClusternya);
        _centerCluster.add(atr);
    }
    

    //END PROCESS STEP2 
    //PROCESS STEP3
   
    /*** ini khusus untuk form Menu_Lakalantas.java ***/
    public void setClusterPoinKhususMenuLakalantas() {
        float[] tempArrayAtrPoin = new float[Atribut.get(0).getAtributItem().length];
        AtributLakaLantas atr;
        for (int a = 0; a < _centerCluster.size(); a++) {
            for (int b = 0; b < Atribut.size(); b++) {
                tempArrayAtrPoin = new float[Atribut.get(0).getAtributItem().length]; //INI TERNYATA PENTING
                for (int c = 0; c < Atribut.get(b).getAtributItem().length; c++) {
                    tempArrayAtrPoin[c] = (float)setPangkat(2, (Atribut.get(b).getAtributItem()[c] - _centerCluster.get(a).getAtributItem()[c]));
                    //System.out.println("Putaran ke "+a+" ClusterPoin ke :"+c+" => "+Atribut.get(b).getAtributItem()[c]+" dan "+_centerCluster.get(a).getAtributItem()[c]+" = "+tempArrayAtrPoin[c]+" , ");
                }
                //System.out.println("");
                atr = new AtributLakaLantas();
                atr.setAtributItem(tempArrayAtrPoin);
                _clusterPoin.add(atr);
            }
        }
        System.out.println("tampilan setClusterPoin() \n");
        int divUIK = 1;//_UIK.size() / Atribut.size();
        int entitasClusterPoin = 0;
        for(int a=0;a<divUIK;a++){
            System.out.println("Next Step : "+a);
            for(int b=0;b<Atribut.size();b++){
                for(int c=0;c<Atribut.get(b).getAtributItem().length;c++){
                    System.out.print(_clusterPoin.get(b+entitasClusterPoin).getAtributItem()[c]+" | ");
                }
                System.out.println("");
            }
            entitasClusterPoin += Atribut.size();
            
        }
        
    }
    
    /*** ini khusus untuk form Menu_Lakalantas.java ***/
    public void set_totalClusterPoinKhususMenuLakalantas() {
        float temptotal = (float)0.0;
        
        int divUIK = 1;
        int entitasClusterPoin = 0;
        for(int a=0;a<divUIK;a++){
            System.out.println("Next Step : "+a);
            for(int b=0;b<Atribut.size();b++){
                for(int c=0;c<Atribut.get(b).getAtributItem().length;c++){
                    System.out.print(_clusterPoin.get(b+entitasClusterPoin).getAtributItem()[c]+" | ");
                    temptotal += _clusterPoin.get(b+entitasClusterPoin).getAtributItem()[c];
                }
                
                _totalClusterPoin.add(temptotal);
                System.out.print(_totalClusterPoin.get(_totalClusterPoin.size()-1));
                temptotal = (float)0.0;
                System.out.println("");
                
            }
            entitasClusterPoin += Atribut.size();
        }
    }

    /*** ini khusus untuk form Menu_Lakalantas.java ***/
    public void set_Allset_totalClusterPoinKhususMenuLakalantas() {
        float[] totalAllClusterSum = new float[Atribut.size()];
        int divUIK = 1;//_UIK.size() / Atribut.size();
        int stepCenterCluster = 0;
        for (int z = 0; z < divUIK; z++) {
            for (int a = 0; a < Atribut.size(); a++) {
                for (int b = 0; b < _clusterPoin.get(stepCenterCluster + a).getAtributItem().length; b++) {
                    totalAllClusterSum[a] += _clusterPoin.get(stepCenterCluster + a).getAtributItem()[b];
                    //System.out.print("index ke "+(stepCenterCluster+a)+" : "+_clusterPoin.get(stepCenterCluster+a).getAtributItem()[b]+" , ");
                }
            }
            stepCenterCluster += Atribut.size();
        }
        
        for (int a = 0; a < totalAllClusterSum.length; a++) {
            _totalAllClusterPoin.add(totalAllClusterSum[a]);
            System.out.println("TOTAL sum Cluster per Horizontal : "+_totalAllClusterPoin.get(a));
        }
    }

    int aaa = 0;
    
    /*** ini khusus untuk form Menu_Lakalantas.java ***/
    public void setRegenerateUIKVersiMenuLakalantas() {
        
         //SEBELUM REGENERATE COPY _UIK LAMA KE _UIK_VIEW 
        _UIK_VIEW = (ArrayList<Float>) _UIK.clone();
        
        
        aaa += 1;
        System.out.println("langkah ke" + aaa);
        int divUIK = 1;//_UIK.size() / Atribut.size();
        System.out.println("hasil DIV UIK ADALAH : "+divUIK);
        int stepCluster = 0;
        System.out.println("yang lama ini adalah : ");
        System.out.println("_totalAllClusterPoin : "+_totalAllClusterPoin.size()+" dan _totalClusterPoin : "+_totalClusterPoin.size());
        for(int a=0;a<_totalClusterPoin.size();a++){
            System.out.println("_totalClusterPoin : "+_totalClusterPoin.get(a));
        }
        for (int z = 0; z < divUIK; z++) {
            for (int a = 0; a < _totalAllClusterPoin.size(); a++) {
                //_UIK.set(stepCluster+a, (_totalClusterPoin.get(stepCluster+a)/_totalAllClusterPoin.get(a)));
                System.out.print((stepCluster + a) + " : " + _UIK.get(stepCluster+a)+" , ");
            }
            stepCluster += _totalAllClusterPoin.size();
            System.out.println("");
        }
        
         for (int a = 0; a < _UIK.size(); a++) {
                //_UIK.set(stepCluster+a, (_totalClusterPoin.get(stepCluster+a)/_totalAllClusterPoin.get(a)));
                System.out.print(_UIK.get(a)+" , ");
         }
        
        System.out.println("");
      
        
        System.out.println();
        stepCluster = 0;
        for (int z = 0; z < divUIK; z++) {
            for (int a = 0; a < _totalAllClusterPoin.size(); a++) {
                //String.format("%.03f", ResultUIK.get(stepUIK + a))
                System.out.println("indexnyaaaa"+(stepCluster + a));
                _UIK.set(stepCluster + a, Float.parseFloat(String.format("%.09f", _totalClusterPoin.get(stepCluster + a) / _totalAllClusterPoin.get(a)).replace(",", ".")));
                System.out.println("UIK ini adalah : " + (stepCluster + a) + " : " + _totalClusterPoin.get(stepCluster + a) +" dibagi "+Float.parseFloat(String.format("%.09f", _totalAllClusterPoin.get(a)).replace(",", ".")));
               
            }
            stepCluster += _totalAllClusterPoin.size();
        }
        
        for (int a = 0; a < _UIK.size(); a++) {
                //_UIK.set(stepCluster+a, (_totalClusterPoin.get(stepCluster+a)/_totalAllClusterPoin.get(a)));
                System.out.print(_UIK.get(a)+" , ");
         }
        
        System.out.println("");
        
        stepCluster = 0;
        System.out.println("yang baru ini adalah : ");
        for (int z = 0; z < divUIK; z++) {
            for (int a = 0; a < _totalAllClusterPoin.size(); a++) {
                //_UIK.set(stepCluster+a, (_totalClusterPoin.get(stepCluster+a)/_totalAllClusterPoin.get(a)));
                System.out.print((stepCluster + a) + " : " + _UIK.get(stepCluster+a));
            }
            stepCluster += _totalAllClusterPoin.size();
            System.out.println("");
        }
        
        //dibalik jadi descending sesuai contoh excel
        /*tempDesckUIK = (ArrayList<Float>) _UIK.clone();
        System.out.println("");
        int b=0;
        for(int a=(tempDesckUIK.size()-1) ; a>=0;){
            //System.out.print(_UIK.get(b)+" tutup");
            tempDesckUIK.set(a, _UIK.get(b));
            b++;
            a--;
        }
        b=0;
         System.out.println("");
        for(int a=(0) ; a<tempDesckUIK.size();){
            System.out.print(tempDesckUIK.get(a)+" dan "+_UIK.get(b)+" tutup");
            a++;
            b++;
        }*/
        //fitur pengubah jadi DESCENDING DIMATIKAN
        //_UIK = (ArrayList<Float>) tempDesckUIK.clone();
        
        double totalAllc = 0;
        for (int a = 0; a < _totalAllClusterPoin.size(); a++) {
            totalAllc += _totalAllClusterPoin.get(a);
            //break;
        }
        System.out.println("total semuanya adalah : " + (int)totalAllc);
        if((int)totalAllc == pembanding1){
            isFinishLoop = true;
        }else{
            if((int)totalAllc == pembanding2){
                isFinishLoop = true;
            }else{
                isFinishLoop = false;
                pembanding2 = pembanding1;
                pembanding1 = (int)totalAllc;
            }
        }
        
    }
    
    public void perbandingan(){
        tableCellHasil tt;
        int divUIK = _UIK_VIEW.size() / Atribut.size();
        int entitasClusterPoin = 0;
        int perindexcolumn = 0;
        float tempPembanding = 0;
        for(int a=0;a<Atribut.size();a++){
          tt = new tableCellHasil();
          for(int b=0;b<divUIK;b++){
              if(b == 0){
                  tempPembanding = _UIK_VIEW.get(entitasClusterPoin+b+perindexcolumn);
                  _indexKhususUIK.add(entitasClusterPoin+b+perindexcolumn);                
                  tt.setColumn(b);
                  tt.setRow(a);
                  
              }else{
                  if(_UIK_VIEW.get(entitasClusterPoin+b+perindexcolumn) > tempPembanding){
                    tempPembanding = _UIK_VIEW.get(entitasClusterPoin+b+perindexcolumn);
                    _indexKhususUIK.set(_indexKhususUIK.size()-1, entitasClusterPoin+b+perindexcolumn);           
                    tt.setColumn(b);
                    tt.setRow(a);
                    
                  }
              }
              
             perindexcolumn += (Atribut.size()-1);
          }  
          tandaTargetUIK.add(tt);
          perindexcolumn = 0;
          entitasClusterPoin += 1;
        } 
        System.out.println("_indexKhususUIK adalah : ");
        System.out.println("");
        for(int a=0;a<_indexKhususUIK.size();a++){
            System.out.print(_indexKhususUIK.get(a)+" , ");
        }
        System.out.println("#############");
        
    }
    //END PROCESS STEP3

    public void resetTempDB() {
        ResultUIK =  new ArrayList<Float>();
        _resultAtribut = new ArrayList<AtributLakaLantas>();
        _centerClusterUIK.clear();
        _resultClusterUIK.clear();
        _resultQuadratUIK.clear();_resultTotalCluster.clear();
        _centerCluster.clear();
        _totalClusterPoin.clear();
        _clusterPoin.clear();
        _totalAllClusterPoin.clear();

    }
    
}
