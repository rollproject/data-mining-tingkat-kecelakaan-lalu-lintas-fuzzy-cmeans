/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package skripsikarina.helper;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import skripsikarina.entity.tableCellHasil;

/**
 *
 * @author donny
 */
public class customCellRender extends DefaultTableCellRenderer  {
    
    public ArrayList<tableCellHasil> _indexKhususUIK = new ArrayList<tableCellHasil>(); 
    public customCellRender(tableCellHasil tch) {
       // this._indexKhususUIK = tch;
    }

    public customCellRender(ArrayList<tableCellHasil> tandaTargetUIK) {
        this._indexKhususUIK = (ArrayList<tableCellHasil>) tandaTargetUIK.clone();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean     isSelected, boolean hasFocus, int row, int column){
        Component c = null;
        c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);   
       for(int a=0;a<_indexKhususUIK.size();a++){
           if(_indexKhususUIK.get(a).getRow() ==  row && _indexKhususUIK.get(a).getColumn() == column){
               c.setBackground(new java.awt.Color(255, 72, 72));
               break;
           }else{
               c.setBackground(new java.awt.Color(255, 255, 72));
           }
           
       }
       /*if(row == 2 && column == 1){
           c.setBackground(new java.awt.Color(255, 72, 72));
       }else{
           c.setBackground(new java.awt.Color(255, 255, 72));
       }*/
       //for(int a=0;a<_indexKhususUIK.size();a++){
            /*if(_indexKhususUIK.get(a).getRow() ==  row && _indexKhususUIK.get(a).getColumn() == column){
                System.out.println(_indexKhususUIK.get(a).getRow()+" "+row);
                System.out.println(_indexKhususUIK.get(a).getColumn()+" "+column);
                c.setBackground(new java.awt.Color(255, 72, 72));
             }else{
                c.setBackground(new java.awt.Color(255, 255, 72));
             }*/
       // }
        return c;
    }
}